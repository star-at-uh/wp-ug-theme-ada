<?php
/*
Plugin Name: WPS Menu Exporter
Description: WPS Menu Exporter permet d'exporter les menus WordPress.
Version: 1.1
Author: WPServeur, NicolasKulka
Author URI: https://wpserveur.net
License: GPL2
*/

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Plugin constants
define( 'WPS_MENU_EXPORTER_VERSION', '1.1' );
define( 'WPS_MENU_EXPORTER_FOLDER', 'wps-menu-exporter' );
define( 'WPS_MENU_EXPORTER_BASENAME', plugin_basename( __FILE__ ) );

define( 'WPS_MENU_EXPORTER_URL', plugin_dir_url( __FILE__ ) );
define( 'WPS_MENU_EXPORTER_DIR', plugin_dir_path( __FILE__ ) );

// Function for easy load files
if ( ! function_exists( 'wpserveur_menu_exporter_load_files' ) ) {
	function wpserveur_menu_exporter_load_files( $dir, $files, $prefix = '' ) {
		foreach ( $files as $file ) {
			if ( is_file( $dir . $prefix . $file . '.php' ) ) {
				require_once( $dir . $prefix . $file . '.php' );
			}
		}
	}
}

// Plugin client classes
wpserveur_menu_exporter_load_files( WPS_MENU_EXPORTER_DIR . 'classes/', array(
	'plugin',
	'review'
) );

if ( ! function_exists( 'plugins_loaded_wps_menu_exporter_plugin' ) ) {
	add_action( 'plugins_loaded', 'plugins_loaded_wps_menu_exporter_plugin' );
	function plugins_loaded_wps_menu_exporter_plugin() {
		new WPS_MENU_EXPORTER();

		load_plugin_textdomain( 'wps-menu-exporter', false, basename( rtrim( dirname( __FILE__ ), '/' ) ) . '/languages' );
	}
}