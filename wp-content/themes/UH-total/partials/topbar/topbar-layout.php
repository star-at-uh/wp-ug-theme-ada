<?php
/**
 * Topbar layout
 *
 * @package UH Total WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<?php wpex_hook_topbar_before(); ?>

	<div id="top-bar-wrap" class="<?php echo esc_attr( wpex_topbar_classes() ); ?>">
		<div id="top-bar" class="clr container">
			<div id="top-bar-content" class="wpex-clr">

				<ul id="menu-very-top" class="top-bar-menu top-bar-left"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-713"><a href="https://manoa.hawaii.edu"><span class="link-inner">Home</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3019"><a href="https://manoa.hawaii.edu/a-z/"><span class="link-inner">A-Z Index</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3020"><a href="https://manoa.hawaii.edu/directory/"><span class="link-inner">Directory</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3021"><a href="https://manoa.hawaii.edu/students/"><span class="link-inner">Students</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3022"><a href="https://manoa.hawaii.edu/faculty-staff/"><span class="link-inner">Faculty and Staff</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1726"><a href="https://manoa.hawaii.edu/admissions/parents.html"><span class="link-inner">Parents</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2659"><a href="https://uhalumni.org/manoa"><span class="link-inner">Alumni</span></a></li>
					<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-716"><a href="https://myuh.hawaii.edu/"><span class="link-inner">MyUH</span></a></li>
				</ul>

				<div id="top-bar-social" class="clr top-bar-right social-style-none">
					<a href="https://twitter.com/UHManoa" title="Twitter" class="wpex-twitter wpex-social-btn wpex-social-btn-no-style"><span class="fa fa-twitter" aria-hidden="true"></span><span class="screen-reader-text">Twitter</span></a><a href="https://www.facebook.com/uhmanoa" title="Facebook" class="wpex-facebook wpex-social-btn wpex-social-btn-no-style"><span class="fa fa-facebook" aria-hidden="true"></span><span class="screen-reader-text">Facebook</span></a><a href="https://instagram.com/uhmanoanews" title="Instagram" class="wpex-instagram wpex-social-btn wpex-social-btn-no-style"><span class="fa fa-instagram" aria-hidden="true"></span><span class="screen-reader-text">Instagram</span></a><a href="http://www.flickr.com/photos/uhmanoa" title="Flickr" class="wpex-flickr wpex-social-btn wpex-social-btn-no-style"><span class="fa fa-flickr" aria-hidden="true"></span><span class="screen-reader-text">Flickr</span></a><a href="http://www.youtube.com/user/UniversityofHawaii" title="Youtube" class="wpex-youtube wpex-social-btn wpex-social-btn-no-style"><span class="fa fa-youtube" aria-hidden="true"></span><span class="screen-reader-text">Youtube</span></a>
				</div><!-- #top-bar-social -->

			</div><!-- #top-bar-content -->
		</div><!-- #top-bar -->
	</div><!-- #top-bar-wrap -->

<?php wpex_hook_topbar_after(); ?>