<?php
/**
 * Footer bottom content
 *
 * @package UH Total WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get footer widgets columns
$columns    = wpex_get_mod( 'footer_widgets_columns', '4' );
$grid_class = apply_filters( 'wpex_footer_widget_col_classes', wpex_grid_class( $columns ) );
$gap        = wpex_get_mod( 'footer_widgets_gap', '30' );

// Classes
$wrap_classes = array( 'clr' );
if ( '1' == $columns ) {
	$wrap_classes[] = 'single-col-footer';
} 
if ( $gap ) {
	$wrap_classes[] = 'gap-'. $gap;
}
$wrap_classes = implode( ' ', $wrap_classes );
$wrap_classes = apply_filters( 'wpex_footer_widget_row_classes', $wrap_classes ); ?>

<div id="footer-widgets" class="wpex-row <?php echo esc_attr( $wrap_classes ); ?>">

	<?php
	// Footer box 1 ?>
	<div class="footer-box span_1_of_4 col col-1">
		<img width="300" height="105" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/uhm-white-seal-nameplate@2x.png" alt="UH Manoa Logo"/>
	</div><!-- .footer-one-box -->

	<?php
	// Footer box 2
	if ( $columns > '1' ) : ?>
		<div class="footer-box <?php echo esc_attr( $grid_class ); ?> col col-2">
		<div class="menu-footer-02-container">
				<ul id="menu-footer-02" class="menu">
					<li id="menu-item-1858" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1858"><a href="https://www.star.hawaii.edu/studentinterface/">Academic Development &amp; Technology</a>
					</li>
					<li id="menu-item-1859" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1859"><a href="http://manoa.hawaii.edu/undergrad/new-aerospace/">Aerospace Studies</a></li>
					<li id="menu-item-1860" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1860"><a href="http://manoa.hawaii.edu/undergrad/new-servicelearn/">Civic and Community Engagement</a></li>
					<li id="menu-item-1861" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1861"><a href="http://manoa.hawaii.edu/undergrad/new-CAA/">Council of Academic Advisors</a></li>
					<li id="menu-item-1862" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1862"><a href="http://manoa.hawaii.edu/undergrad/new-exploratory/">Exploratory Program</a></li>
					<li id="menu-item-1863" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1863"><a href="http://manoa.hawaii.edu/undergrad/new-finlit/">Financial Literacy Program</a></li>
					<li id="menu-item-1864" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1864"><a href="http://manoa.hawaii.edu/undergrad/new-freshman/">First-Year Programs</a></li>
					<li id="menu-item-1865" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1865"><a href="http://manoa.hawaii.edu/undergrad/new-honors/">Honors Program</a></li>
				</ul>
			</div>
		</div><!-- .footer-one-box -->
	<?php endif; ?>
	
	<?php
	// Footer box 3
	if ( $columns > '2' ) : ?>
		<div class="footer-box <?php echo esc_attr( $grid_class ); ?> col col-3 ">
		<div class="menu-footer-03-container">
				<ul id="menu-footer-03" class="menu"><li id="menu-item-1866" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1866"><a href="http://manoa.hawaii.edu/undergrad/new-IS/">Interdisciplinary Studies</a></li>
					<li id="menu-item-1867" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1867"><a href="http://manoa.hawaii.edu/undergrad/new-Learning/">Learning Assistance Center</a></li>
					<li id="menu-item-1868" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1868"><a href="http://manoa.hawaii.edu/undergrad/new-MAC/">Mānoa Advising Center</a></li>
					<li id="menu-item-1869" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1869"><a href="http://manoa.hawaii.edu/undergrad/new-catalog-office/">Mānoa Catalog</a></li>
					<li id="menu-item-1870" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1870"><a href="http://manoa.hawaii.edu/undergrad/new-horizons/">Mānoa Horizons</a></li>
					<li id="menu-item-1871" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1871"><a href="http://manoa.hawaii.edu/undergrad/new-sophomore/">Mānoa Sophomore Experience</a></li>
					<li id="menu-item-1872" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1872"><a href="http://manoa.hawaii.edu/undergrad/new-Transfer/">Mānoa Transfer Coordination Center</a></li>
					<li id="menu-item-1873" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1873"><a href="http://manoa.hawaii.edu/undergrad/new-armyrotc/">Military Science</a></li>
				</ul>
			</div>
		</div><!-- .footer-one-box -->
	<?php endif; ?>

	<?php
	// Footer box 4
	if ( $columns > '3' ) : ?>
		<div class="footer-box <?php echo esc_attr( $grid_class ); ?> col col-4">
		<div class="menu-footer-04-container">
			<ul id="menu-footer-04" class="menu"><li id="menu-item-1874" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1874"><a href="http://manoa.hawaii.edu/undergrad/new-PAC/">Pre-Health / Pre-Law Advising</a></li>
				<li id="menu-item-1875" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1875"><a href="http://manoa.hawaii.edu/undergrad/new-schedule/">Schedule – Academic</a></li>
				<li id="menu-item-1876" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1876"><a href="http://manoa.hawaii.edu/undergrad/new-fellowships/">Scholarships and Fellowships</a></li>
				<li id="menu-item-1877" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1877"><a href="http://manoa.hawaii.edu/undergrad/new-SAAS/">Student Athlete Academic Services</a></li>
				<li id="menu-item-1878" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1878"><a href="http://manoa.hawaii.edu/undergrad/new-ssc/">Student Success Center</a></li>
				<li id="menu-item-1879" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1879"><a href="http://manoa.hawaii.edu/undergrad/new-sss/">Student Support Services</a></li>
				<li id="menu-item-1880" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1880"><a href="http://manoa.hawaii.edu/undergrad/new-urop/">Undergraduate Research Opportunities</a></li>
				<li id="menu-item-1881" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1881"><a href="http://manoa.hawaii.edu/undergrad/new-showcase/">Undergraduate Showcase</a></li>
			</ul>
		</div>
		</div><!-- .footer-box -->
	<?php endif; ?>

</div><!-- #footer-widgets -->