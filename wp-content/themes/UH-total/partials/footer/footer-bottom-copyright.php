<?php
/**
 * Footer bottom content
 *
 * @package UH Total WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get copyright info
$copyright = wpex_get_mod( 'footer_copyright_text', 'The University of Hawaiʻi is an <a href="http://hawaii.edu/offices/eeo/policies.php?policy=antidisc">equal opportunity/affirmative action institution</a><br> ©[current_year] University of Hawaiʻi at Mānoa • 2500 Campus Road • Honolulu, HI 96822 • (808) 956-8111' );

// Translate the theme option
$copyright = wpex_translate_theme_mod( 'footer_copyright_text', $copyright );

// Return if there isn't any copyright content to display
if ( ! $copyright ) {
	return;
} ?>

<div id="copyright" class="clr"<?php wpex_aria_landmark( 'copyright' ); ?>>
	<?php echo do_shortcode( wp_kses_post( $copyright ) ); ?>
</div><!-- #copyright -->