<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_ug_theme_ada');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_Q9)yDz=lCeSrxCXTFU^L5It0.@Ao{}3sy:@qJq* IR4QRYZ0Z)#%smL:j4uS6. ');
define('SECURE_AUTH_KEY',  'Zx@_{]O%PCA-jibm_bQiJ5/gi<hhm${2BKA< 8eB&UL0RV#URDGo:5qt%1P8zg6k');
define('LOGGED_IN_KEY',    'uQ,kdFgM|moTgGzP{CN>fCx)Skf6nObANC!^Ykk,0OhUsi`{m_zj3&,i4dZa `D^');
define('NONCE_KEY',        '`8R50&HxQ+^)&x?T~jAi*A{b?auW:[}xtZF;L_mbh#wE!] o-=+@6~`Ya-rja}U-');
define('AUTH_SALT',        'FfELH9dM:IlBaTwPXJ}ud=>1AlZ.|,SH?93pOpA01azkvQpm/|wOjvbsb7~/q*B-');
define('SECURE_AUTH_SALT', 'q2V8jGQ$ J^wJ y~#b9//+fA)#D&1woVV`:eM/amZ()}^Q!SU:0wTN&hngL<QvTU');
define('LOGGED_IN_SALT',   'BTJPBz>E|!Vio9qb3;#VOrGUWqusT1obJ#&6@1CTb@P.cHm`{y.iaS.Ef?GQDL7N');
define('NONCE_SALT',       '+MfXz@z+b9wuKzh>53!/e:NIk&MCo5H?`))zeO=Vw%Q`?O}%3ecHpgS+N=ulivWs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
